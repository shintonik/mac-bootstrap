#!/bin/bash

set -e

ENVIRONMENTS=(work private)

select choice in private work quit
do
     case "$choice" in
      quit)  break ;;
        "")  echo "$REPLY: Invalid choice" ;;
         *)  system_env=$choice
             break ;;
   esac
done

SRC_DIRECTORY="$HOME/Desktop/provisioning"
ANSIBLE_DIRECTORY="$SRC_DIRECTORY/ansible"

# Download and install Command Line Tools
if [[ ! -x /usr/bin/gcc ]]; then
    echo "Info   | Install   | xcode"
    xcode-select --install
fi

# Download and install Homebrew
if [[ ! -x /usr/local/bin/brew ]]; then
    echo "Info   | Install   | homebrew"
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# Modify the PATH
export PATH=/usr/local/bin:$PATH

# Download and install git
if [[ ! -x /usr/local/bin/git ]]; then
    echo "Info   | Install   | git"
    brew install git
fi

# Update brew package informations
echo "Info   | update brew"
brew update

# Upgrade all existing packages
echo "Info   | upgrade existing brew packages"
brew upgrade

# Download and install python
if [[ ! -x /usr/local/bin/python ]]; then
    echo "Info   | Install   | python"
    brew install python --framework --with-brewed-openssl
fi

# Download and install Ansible
if [[ ! -x /usr/local/bin/ansible ]]; then
    brew install ansible
fi

# Create the code directory
mkdir -p $SRC_DIRECTORY

# Clone down ansible
if [[ ! -d $ANSIBLE_DIRECTORY ]]; then
    git clone https://shintonik@bitbucket.org/shintonik/mac-$system_env-ansible.git $ANSIBLE_DIRECTORY
else
	cd $ANSIBLE_DIRECTORY
	git pull origin master
fi

# Provision the box
cd $SRC_DIRECTORY
ansible-playbook --ask-sudo-pass -i ansible/inventories/local ansible/provisioning.yml
